<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h3>Form Pendaftaran</h3>
    <form action="/kirim" method="POST">
        @csrf
        <label>First Name</label><br>
        <input type="text" name="nama"><br>
        <label>Last name: </label> <br>
        <input type="text" name="name1"> <br><br>

        <label>Gender: </label> <br><br>
        <input type="radio" name="gender" value="Male">Male <br>
        <input type="radio" name="gender" value="Female">Female <br>
        <input type="radio" name="gender" value="Other">Other<br><br>

        <label>Nationality: </label> <br> <br>
        <select  name="nationality" id="">
            <option value="Indonesia">Indonesia</option>
            <option value="amerika">Amerika</option>
            <option value="china">china</option>
        </select>
        <br><br>

        <label>Language Spoken: </label> <br><br>
        <input type="checkbox" name="language">Bahasa Indonesia <br>
        <input type="checkbox" name="language">English<br>
        <input type="checkbox" name="language"> Other<br><br>

        <label>Bio: </label> <br><br>
        <textarea rows="10"></textarea>
        <!-- <input type="text" name="bio" value=""> -->

        <br> <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>